"""
views.py

URL route handlers

Note that any handler params must match the URL route params.
For example the *say_hello* handler, handling the URL route '/hello/<username>',
  must be passed *username* as the argument.

"""
from functools import wraps
import urlparse
import urllib
import urllib2
import json
import re

from google.appengine.api import mail
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
from google.appengine.ext import ndb, deferred

from flask import request, render_template, flash, url_for, redirect, session
from flask_cache import Cache

from application import app
from forms import ContactForm
from models import Contact, Address, PhoneNumber, User

from app_settings import FACEBOOK_APP_ID, FACEBOOK_APP_SECRET, GOOGLE_SERVICE_EMAIL

# Flask-Cache (configured to use App Engine Memcache API)
cache = Cache(app)

def login_required(func):
    """Requires standard login credentials"""
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not 'user' in session:
            return redirect(url_for('login', next=request.url))
        return func(*args, **kwargs)
    return decorated_view


def add_fb_friends_to_contacts(access_token, key):
    """ Task to be run by deferred.defer.
        To be run after authenication is successful 
        Need the Oauth access token and the ndb User key
    """        
    # adding FB friends
    user = key.get()
    
    friends = json.load(urllib.urlopen(
        "https://graph.facebook.com/me/friends?" +
        urllib.urlencode(dict(fields="id,first_name,last_name", access_token=access_token))))

    contacts = []
    for friend in friends['data']:
        contact = Contact(first_name = friend['first_name'],
                  last_name = friend['last_name'],
                  facebook_id = friend['id'],
                  parent = key)
        contacts.append(contact)
    try:
        contact_keys = ndb.put_multi(contacts)
    except CapabilityDisabledError:
        if user.email:
            mail.send_mail(sender=GOOGLE_SERVICE_EMAIL,
              to=user.email,
              subject="Contacts import failed",
              body="""Contacts import failed""")   

    if user.email:
        mail.send_mail(sender=GOOGLE_SERVICE_EMAIL,
                       to=user.email,
                       subject="Contacts imported successfully - %s" % len(contact_keys),
                       body="""Contacts imported successfully - %s""" % len(contact_keys))

def login():
    """ Login a user using facebook connect 
        If the user logs in for the first time then start a Task queue to import friends from FB
        After authenicating create a session with the user details
    """
    if 'user' in session:
        return redirect(url_for('home'))
    else:
        verification_code = request.args.get('code', '')
        home_redirect_uri = re.sub(r'^http://www.', 'http://', url_for('login', _external=True)) # Fix for http:// vs http://www issue
        scope = "email" # ask for permission to access the email from facebook profile
        args = dict(client_id = FACEBOOK_APP_ID,
                    redirect_uri = home_redirect_uri,
                    scope = scope)

        if verification_code:
            # You have received the authentication code
            args["client_secret"] = FACEBOOK_APP_SECRET
            args["code"] = verification_code
            response = urlparse.parse_qs(urllib.urlopen(
                    "https://graph.facebook.com/oauth/access_token?" +
                    urllib.urlencode(args)).read())
                    
            access_token = response["access_token"][-1]

            # Download the user profile and cache a local instance of the
            # basic profile information
            profile = json.load(urllib.urlopen(
                "https://graph.facebook.com/me?" +
                urllib.urlencode(dict(access_token=access_token))))
            
            user = User.get_by_id(str(profile["id"]))
            if not user:
                # User logged in for the first time
                user = User(id=str(profile["id"]),
                            name=profile["name"], 
                            access_token=access_token,
                            profile_url=profile["link"])
                if "email" in profile:
                    user.email = profile["email"]
                user.put()
                # Push Task queue to import contacts and send email because the user has accessed our app for the first time
                key = user.key
                deferred.defer(add_fb_friends_to_contacts, access_token, key) # run this task via the default push task queue
            elif user.access_token != access_token:
                # User logged in again and the access token has changed
                user.access_token = access_token
                user.put()
            # Create flask session for the user
            session['user'] = dict(name = user.name, id=user.key.id(), profile_url=user.profile_url, access_token=user.access_token)
            
            return redirect(url_for('home'))
        else:
            # You have NOT got the authentication code. Hence redirect to facebook login authorization url
            return redirect("https://graph.facebook.com/oauth/authorize?" +
                             urllib.urlencode(args))

def home():
    if 'user' in session:
        return redirect(url_for('list_contacts'))
    else:
        return render_template('index.html')

@login_required
def logout():
    logout_url = "https://www.facebook.com/logout.php?next=%s&access_token=%s" % (url_for('home', _external=True), session['user']['access_token']) 
    session.pop('user', None)
    return redirect(logout_url)
    
@login_required
def list_contacts():
    """ List all Contacts """
    
    user_id = session['user']['id']
    contacts = Contact.get_contacts(user_id)
    form = ContactForm()
    
    # When the user posts via the new Contact modal, validate and save the data to the datastore
    if request.method == "POST":
        if form.validate_on_submit():
            contact = Contact(first_name = form.first_name.data,
                              last_name = form.last_name.data,
                              email_ids = form.email_ids.data,
                              phone_numbers = [ PhoneNumber(**kwargs) for kwargs in form.phone_numbers.data if kwargs['number'] ],
                              addresses = [ Address(**kwargs) for kwargs in form.addresses.data  if kwargs['street'] or kwargs['city'] ],
                              parent = User.get_user_key(user_id))
            try:
                contact.put()
                contact_id = contact.key.id()
                flash(u'Contact %s successfully saved.' % contact_id, 'success')
                return redirect(url_for('list_contacts'))
            except CapabilityDisabledError:
                flash(u'App Engine Datastore is currently in read-only mode.', 'info')
                return redirect(url_for('list_contacts'))
        else:
            flash(u'New Contact Creation failed.', 'error')
    return render_template('list_contacts.html', contacts=contacts, form=form)

@login_required
def view_contact(contact_id):    
    """ Detailed read-only view of contact given a contact id """ 

    user_id = session['user']['id']
    contact = Contact.get_contact(user_id, contact_id)
    return render_template('view_contact.html', contact=contact)
    
@login_required
def edit_contact(contact_id):
    """ Edit a contact object given a contact id """
    
    user_id = session['user']['id']
    contact = Contact.get_contact(user_id, contact_id)
    form = ContactForm(obj=contact)
    form.restructure_form(contact) # min entries not working in wtforms. So I am handling the dynamic resizing of the form entries
    if request.method == "POST":
        if form.validate_on_submit():
            contact.first_name = form.first_name.data
            contact.last_name = form.last_name.data
            contact.email_ids = form.email_ids.data
            contact.phone_numbers = [ PhoneNumber(**kwargs) for kwargs in form.phone_numbers.data  if kwargs['number']]
            contact.addresses = [ Address(**kwargs) for kwargs in form.addresses.data if kwargs['street'] or kwargs['city']]           
            contact.put()
            flash(u'Contact %s successfully saved.' % contact_id, 'success')
            return redirect(url_for('list_contacts'))
    return render_template('edit_contact.html', contact=contact, form=form)

@login_required
def delete_contact(contact_id):
    """Delete an contact object given a contact id"""
    
    user_id = session['user']['id']
    try:
        Contact.delete_contact(user_id, contact_id)
        flash(u'Contact %s successfully deleted.' % contact_id, 'success')
        return redirect(url_for('list_contacts'))
    except CapabilityDisabledError:
        flash(u'App Engine Datastore is currently in read-only mode.', 'info')
        return redirect(url_for('list_contacts'))

@cache.cached(timeout=60)
def cached_contacts():
    """This view should be cached for 60 sec"""
    
    user_id = session['user']['id']
    contacts = Contact.get_contacts(user_id)
    return render_template('list_contacts_cached.html', contacts=contacts)

def warmup():
    """App Engine warmup handler
    See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests

    """
    return ''

