"""
urls.py

URL dispatch route mappings and error handlers

"""
from flask import render_template

from application import app
from application import views


## URL dispatch rules
# App Engine warm up handler
# See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'warmup', view_func=views.warmup)

# Home page
app.add_url_rule('/', 'home', view_func=views.home)

# Login page
app.add_url_rule('/logout', 'logout', view_func=views.logout)

#Logout page
app.add_url_rule('/login', 'login', view_func=views.login)

# Contacts list page
app.add_url_rule('/contacts', 'list_contacts', view_func=views.list_contacts,  methods=['GET', 'POST'])

# View a contact
app.add_url_rule('/contacts/<int:contact_id>', 'view_contact', view_func=views.view_contact, methods=['GET'])

# Edit a contact
app.add_url_rule('/contacts/<int:contact_id>/edit', 'edit_contact', view_func=views.edit_contact, methods=['GET', 'POST'])

# Delete a contact
app.add_url_rule('/contacts/<int:contact_id>/delete', view_func=views.delete_contact, methods=['POST'])

# Contacts list page (cached)
app.add_url_rule('/contacts/cached', 'cached_contacts', view_func=views.cached_contacts, methods=['GET'])

## Error handlers
# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

