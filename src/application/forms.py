"""
forms.py

Web forms based on Flask-WTForms

See: http://flask.pocoo.org/docs/patterns/wtforms/
     http://wtforms.simplecodes.com/

"""

from flaskext import wtf
from flaskext.wtf import validators, fields
from wtforms.ext.appengine.ndb import model_form

from .models import Contact, Address, PhoneNumber

AddressForm = model_form(Address, wtf.Form,
                         field_args={
                                        'type' : dict(validators = [validators.Optional()]),
                                        'street' : dict(validators = [validators.Length(max=100, message="Street field cannot be more than %(max)d")]),
                                        'city' : dict(validators = [validators.Length(max=30, message="City field cannot be more than %(max)d")]),
                                    })

PhoneNumberForm = model_form(PhoneNumber, wtf.Form, 
                             field_args={
                                            'type' : dict(validators = [validators.Optional()]),
                                            'number' : dict(validators = [validators.Optional(), validators.Regexp(regex=r'^\d{9,14}$', message=u'Invalid phone number.Can only contain numbers and should be 9-14 chars in length.')])
                                        }
                            )            

BaseContactForm = model_form(Contact, wtf.Form, only =('first_name', 'last_name', 'email_ids'), 
                         field_args={
                                        'first_name' : dict(validators = [validators.Required(), validators.Length(max=30, message="First Name cannot be more than %(max)d")]),
                                        'last_name' : dict(validators = [validators.Required(), validators.Length(max=30, message="Last Name cannot be more than %(max)d")]),
                                        'email_ids' : dict(validators = [validators.Length(max=5, message="Not more than %(max)d email id entries")])
                                    }
                        )
                        
class ContactForm(BaseContactForm):
    phone_numbers = fields.FieldList(fields.FormField(PhoneNumberForm), 'PhoneNumber', min_entries=2, max_entries=6)
    addresses = fields.FieldList(fields.FormField(AddressForm), 'Address', min_entries=2, max_entries=6)
        
    def _adding_repeated_entries(self, fieldlist, fieldlistdata):
        """ Added the required number of n free entries upto max entries """ 
        current_entries = len(fieldlist.entries)
        current_data_items = len(fieldlistdata)
        entries_free = current_data_items - current_entries
        entries_to_add = fieldlist.min_entries + entries_free
        if current_entries + entries_to_add > fieldlist.max_entries:
            entries_to_add -= (current_entries + entries_to_add) - fieldlist.max_entries
        for i in range(entries_to_add):
            fieldlist.append_entry()
    
    def restructure_form(self, contact):
        """ Restructure the form """
        self._adding_repeated_entries(self.phone_numbers, contact.phone_numbers)
        self._adding_repeated_entries(self.addresses, contact.addresses)