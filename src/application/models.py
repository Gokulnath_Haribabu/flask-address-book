"""
models.py

App Engine datastore models

"""

from google.appengine.ext import ndb


class TimeStampedModel(ndb.Model):
    """ Base class to be used by other entities to provide Timestamping to them """
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
        
class Address(ndb.Model):
    """ Address Entity used as Structured Property by Contact """
    type = ndb.StringProperty(required=True, choices=['home', 'work', 'other'])
    street = ndb.StringProperty()
    city = ndb.StringProperty()

class PhoneNumber(ndb.Model):
    """ PhoneNumber Entity used as Structured Property by Contact """
    type = ndb.StringProperty(required=True, choices=['home', 'work', 'mobile', 'other'])
    number = ndb.StringProperty()

class User(TimeStampedModel):
    """ User Entity used to save the Facebook login details """
    # FB ID    implicit set when adding entries using id column
    name = ndb.StringProperty(required=True)
    email = ndb.StringProperty()
    profile_url = ndb.StringProperty(required=True)
    access_token = ndb.StringProperty(required=True)  #fb OAUTH access token
    
    @classmethod
    def get_user_key(cls, user_id):
        return ndb.Key(cls, user_id)

class Contact(TimeStampedModel):
    """ Contact Model """
    first_name = ndb.StringProperty(required=True)
    last_name = ndb.StringProperty(required=True)
    facebook_id = ndb.StringProperty()
    email_ids =  ndb.StringProperty(repeated = True)
    addresses = ndb.StructuredProperty(Address, repeated=True)
    phone_numbers = ndb.StructuredProperty(PhoneNumber, repeated=True)
    #Note :
    # Parent is set as User Key when contacts are added to enable the ancestor query for strong consistency
    
    @classmethod
    def get_contacts(cls, user_id):
        key = ndb.Key(User, user_id)
        return cls.query(ancestor=key).order(-cls.updated)
        
    @classmethod
    def get_contact(cls, user_id, contact_id):
        key = ndb.Key(User, user_id)
        return cls.get_by_id(id=contact_id, parent=key)
        
    @classmethod
    def delete_contact(cls, user_id, contact_id):
        key = ndb.Key(User, user_id)
        contact = cls.get_by_id(id=contact_id, parent=key)
        contact.key.delete()