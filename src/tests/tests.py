#!/usr/bin/env python
# encoding: utf-8
"""
tests.py

TODO: These tests need to be updated to support the Python 2.7 runtime
      Broken !!! Need to make it work in the latest GAE SDK version
"""
import os
import unittest
import re

from google.appengine.ext import testbed

from application import app
from application import models

class AddressBookTestCase(unittest.TestCase):
    def setUp(self):
        # Flask apps testing. See: http://flask.pocoo.org/docs/testing/
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        self.app = app.test_client()
        # Setups app engine test bed. See: http://code.google.com/appengine/docs/python/tools/localunittesting.html#Introducing_the_Python_Testing_Utilities
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_user_stub()
        self.testbed.init_memcache_stub()
        self.user_dict = dict(name = "tinkywinky", id="100", profile_url="la-la", access_token="makkapakka")
        
    def tearDown(self):
        self.testbed.deactivate()
       
    def setup_user(self):
        with self.app.session_transaction() as sess:
            user = models.User(**self.user_dict) # add dummy user to ndb ( needed to enter contacts as a parent entity 
            user.put()
            sess['user'] = self.user_dict
            
    def test_home_page(self):
        rv = self.app.get('/')
        assert rv.status == '200 OK'    
        
    def test_login_link_present(self):
        rv = self.app.get('/')
        assert 'Login using FB' in rv.data    
        
    def test_redirect_to_contacts_if_session_valid(self):
        self.setup_user()
        rv = self.app.get('/login', follow_redirects=True)
        assert 'All Contacts' in rv.data

    def test_empty_contacts_page(self):
        self.setup_user()
        rv =self.app.get('/contacts')
        assert 'No contacts yet' in rv.data
        
    def test_insert_contact_check_if_exists(self):         
        self.setup_user()
        rv = self.app.post('/contacts', data={'first_name' : "Paul", 
                                       'last_name' : "Graham", 
                                       'email_ids' : "\n".join(['a@b.com', 'b@c.com']), 
                                       'phone_numbers-0-type': u'other',
                                       'phone_numbers-0-number': u'01234567890',
                                       'addresses-0-type' : u'home',
                                       'addresses-0-street' : u'10, downing street',
                                       'addresses-0-city' : u'London',
                                       }, follow_redirects=True)

        m = re.search(r'Contact (\d+) successfully saved', rv.data, flags=re.MULTILINE)
        contact_id = m.group(1) # gets the contact id
        
        assert 'Paul' in rv.data
        assert 'Graham' in rv.data
        
        rv = self.app.get('/contacts/%s' % contact_id)

        assert 'Paul' in rv.data
        assert 'Graham' in rv.data
        assert 'a@b.com' in rv.data
        assert 'b@c.com' in rv.data
        assert '01234567890' in rv.data
        assert '10, downing street' in rv.data
        assert 'London' in rv.data

    def test_insert_contact_and_delete_check(self):         
        self.setup_user()
        rv = self.app.post('/contacts', data={'first_name' : "TEST", 
                                       'last_name' : "TESTER", 
                                       'email_ids' : [''], 
                                       'phone_numbers-0-type': u'other',
                                       'phone_numbers-0-number': u'01234567890',
                                       'addresses-0-type' : u'home',
                                       'addresses-0-street' : u'10, downing street',
                                       'addresses-0-city' : u'London',
                                       }, follow_redirects=True)
        
        m = re.search(r'Contact (\d+) successfully saved', rv.data, flags=re.MULTILINE)
        contact_id = m.group(1) # gets the contact id
        
        rv = self.app.post('/contacts/%s/delete' % contact_id, follow_redirects=True)
        assert 'Contact %s successfully deleted' % contact_id in rv.data
        assert 'TESTER' not in rv.data

    def test_insert_contact_and_update_check(self):         
        self.setup_user()
        rv = self.app.post('/contacts', data={'first_name' : "TEST", 
                                       'last_name' : "USER", 
                                       'email_ids' : [''], 
                                       'phone_numbers-0-type': u'other',
                                       'phone_numbers-0-number': u'01234567890',
                                       'addresses-0-type' : u'home',
                                       'addresses-0-street' : u'10, downing street',
                                       'addresses-0-city' : u'London',
                                       }, follow_redirects=True)
        
        m = re.search(r'Contact (\d+) successfully saved', rv.data, flags=re.MULTILINE)
        contact_id = m.group(1) # gets the contact id
        
        rv = self.app.post('/contacts/%s/edit' % contact_id, data={'first_name' : "TEST", 
                                       'last_name' : "USER", 
                                       'email_ids' : [''], 
                                       'phone_numbers-0-type': u'other',
                                       'phone_numbers-0-number': u'9999999999',
                                       'addresses-0-type' : u'home',
                                       'addresses-0-street' : u'10, downing street',
                                       'addresses-0-city' : u'Cardiff',
                                       }, follow_redirects=True)
        assert 'Contact %s successfully saved' % contact_id in rv.data
        assert 'TESTER' not in rv.data
        
        rv = self.app.get('/contacts/%s' % contact_id)
        
        assert '01234567890' not in rv.data
        assert 'London' not in rv.data
    
    def test_insert_contact_invalid_entries(self):         
        self.setup_user()
        rv = self.app.post('/contacts', data={'first_name' : ''.join([str(x) for x in range(35)]), # > 30 chars 
                                       'last_name' : ''.join([str(x) for x in range(35)]), # > 30 chars
                                       'email_ids' : "\n".join(['a@b.com', 'b@c.com', 'c@d.com', 'd@e.com', 'e@f.com', 'f@g.com']), # > 5 entries 
                                       'phone_numbers-0-type': u'wrong',  # not in choices 
                                       'phone_numbers-0-number': ''.join([str(x) for x in range(17)]), # numbers > 14
                                       'phone_numbers-1-type': u'wrong',  # not in choices 
                                       'phone_numbers-1-number': u'aaa0192xse', # numbers alphanumeric
                                       'addresses-0-type' : u'wrong', # not in choices
                                       'addresses-0-street' : ''.join([str(x) for x in range(105)]), # > 100 chars
                                       'addresses-0-city' : ''.join([str(x) for x in range(35)]), # > 30 chars
                                       }, follow_redirects=True)
        
        assert 'No contacts yet' in rv.data
        assert 'First Name cannot be more than %s' % 30 in rv.data
        assert 'Last Name cannot be more than %s' % 30 in rv.data
        assert 'Not more than %s email id entries' % 5 in rv.data
        assert 'Invalid phone number.Can only contain numbers and should be 9-14 chars in length.' in rv.data
        assert 'Street field cannot be more than %s' % 100 in rv.data
        assert 'City field cannot be more than %s' % 30 in rv.data
        assert 'Not a valid choice' in rv.data
    
    def test_update_contact_invalid_entries(self):         
        self.setup_user()
        
        rv = self.app.post('/contacts', data={'first_name' : "TEST", 
                                       'last_name' : "USER", 
                                       'email_ids' : [''], 
                                       'phone_numbers-0-type': u'other',
                                       'phone_numbers-0-number': u'01234567890',
                                       'addresses-0-type' : u'home',
                                       'addresses-0-street' : u'10, downing street',
                                       'addresses-0-city' : u'London',
                                       }, follow_redirects=True)
        
        m = re.search(r'Contact (\d+) successfully saved', rv.data, flags=re.MULTILINE)
        contact_id = m.group(1) # gets the contact id
        
        rv = self.app.post('/contacts/%s/edit' % contact_id, data={'first_name' : ''.join([str(x) for x in range(35)]), # > 30 chars 
                                       'last_name' : ''.join([str(x) for x in range(35)]), # > 30 chars
                                       'email_ids' : "\n".join(['a@b.com', 'b@c.com', 'c@d.com', 'd@e.com', 'e@f.com', 'f@g.com']), # > 5 entries 
                                       'phone_numbers-0-type': u'wrong',  # not in choices 
                                       'phone_numbers-0-number': ''.join([str(x) for x in range(17)]), # numbers > 14
                                       'phone_numbers-1-type': u'wrong',  # not in choices 
                                       'phone_numbers-1-number': u'aaa0192xse', # numbers alphanumeric
                                       'addresses-0-type' : u'wrong', # not in choices
                                       'addresses-0-street' : ''.join([str(x) for x in range(105)]), # > 100 chars
                                       'addresses-0-city' : ''.join([str(x) for x in range(35)]), # > 30 chars
                                       }, follow_redirects=True)
        
        assert 'First Name cannot be more than %s' % 30 in rv.data
        assert 'Last Name cannot be more than %s' % 30 in rv.data
        assert 'Not more than %s email id entries' % 5 in rv.data
        assert 'Invalid phone number.Can only contain numbers and should be 9-14 chars in length.' in rv.data
        assert 'Street field cannot be more than %s' % 100 in rv.data
        assert 'City field cannot be more than %s' % 30 in rv.data
        assert 'Not a valid choice' in rv.data
    
    def test_insert_contact(self):         
        self.setup_user()
        rv = self.app.post('/contacts', data={'first_name' : "TEST", 
                                       'last_name' : "TESTER", 
                                       'email_ids' : [''], 
                                       'phone_numbers-type-0': u'other',
                                       'phone_numbers-number-0': u'01234567890',
                                       'addresses-type-0' : u'home',
                                       'addresses-street-0' : u'10, downing street',
                                       'addresses-city-0' : u'London',
                                       }, follow_redirects=True)
        assert 'successfully saved' in rv.data

    def test_404(self):
        rv = self.app.get('/missing')
        assert rv.status == '404 NOT FOUND'
        assert '<h1>Not found</h1>' in rv.data

    
if __name__ == '__main__':
    unittest.main()
