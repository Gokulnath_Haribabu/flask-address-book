# This is my README

Toy application to manage your Address Book.

* The login is done via facebook authentication
* For the first time when a user logs-in, the user's facebook friends will be pulled into the contacts database and an email will be sent upon import completion
* Basic CRUD functionality provided
* Google App Engine, ndb, Flask, Facebook Api, twitter-bootstrap
* The demo runs at http://flaskaddressbook1.appspot.com/


Uses scaffolding provided by https://github.com/kamalgill/flask-appengine-template